package ca.csf.mobile2.tp2.question.data

/**
 * Énumération des différents états possibles pour la demande de question
 */
enum class AskQuestionActivityState {
    ASKQUESTION,
    ANSWERQUESTION,
    DISPLAYRESULTS,
    LOADING,
    ERROR_RETRY
}
