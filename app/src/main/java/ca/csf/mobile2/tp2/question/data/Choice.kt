package ca.csf.mobile2.tp2.question.data

/**
 * Énumaration des différents types de réponses possibles à une question
 */
enum class Choice {
    FIRSTCHOICE,
    SECONDCHOICE
}