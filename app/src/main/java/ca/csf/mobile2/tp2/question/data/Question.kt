package ca.csf.mobile2.tp2.question.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

/**
 * Objet de données représentant une question
 * @property choice1 le texte représentant le choix 1
 * @property choice2 le texte représentant le choix 2
 * @property id l'identifiant unique de la question
 * @property nbChoice1 Le nombre de fois que le choix 1 a été choisi
 * @property nbChoice2 Le nombre de fois que le choix 2 a été choisi
 * @property text la question à poser
 */
@Parcelize
class Question(@JsonProperty("choice1") val choice1 : String,
               @JsonProperty("choice2") val choice2 : String,
               @JsonProperty("id") val id : String,
               @JsonProperty("nbChoice1") val nbChoice1 : Int,
               @JsonProperty("nbChoice2") val nbChoice2 : Int,
               @JsonProperty("text") val text : String) : Parcelable
