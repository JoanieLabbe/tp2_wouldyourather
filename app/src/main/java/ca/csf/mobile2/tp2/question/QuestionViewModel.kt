package ca.csf.mobile2.tp2.question

import android.os.Parcelable
import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import kotlinx.android.parcel.Parcelize
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import androidx.databinding.BindingAdapter
import ca.csf.mobile2.tp2.question.data.AskQuestion
import ca.csf.mobile2.tp2.question.data.AskQuestionActivityState
import ca.csf.mobile2.tp2.question.data.Question

/**
 * Constante qui permet de faire la multiplication pour le pourcentage
 */
private const val MULTIPLIER: Int = 100

/**
 * Le VueModel pour la vue AskQuestion
 * @param askQuestion le modèle de AskQuestionActivity
 */
@Parcelize
class QuestionViewModel(private val askQuestion: AskQuestion)
    : BaseObservable(), Parcelable {

    /**
     * La question contenu dans askQuestion
     */
    var question: Question
        get() { return askQuestion.response }
        set(value) {
            askQuestion.response = value
            notifyChange()
        }

    /**
     * L'état contenu dans askQuestion
     */
    var currentState : AskQuestionActivityState
        get() { return askQuestion.currentState }
        set(value) {
            askQuestion.currentState = value
            notifyChange()
        }

    /**
     * La question à afficher dans la vue
     */
    val text: String
        @Bindable
        get() { return askQuestion.response.text }

    /**
     * Le choix 1 à afficher dans la vue
     */
    val choice1: String
        @Bindable
        get() { return askQuestion.response.choice1 }

    /**
     * Le choix 2 à afficher dans la vue
     */
    val choice2: String
        @Bindable
        get() { return askQuestion.response.choice2 }

    /**
     * le pourcentage de réponses qui ont choisi le choix 1
     */
    val choice1Percent: String
        @Bindable
        get() {
            val percent = getPercent(askQuestion.response.nbChoice1)
            return "$percent%"
        }

    /**
     * Le pourcentage de réponses qui ont choisi le choix 2
     */
    val choice2Percent: String
        @Bindable
        get() {
            val percent = getPercent(askQuestion.response.nbChoice2)
            return "$percent%"
        }

    /**
     * Converti le nombre de réponse en pourcentage pour l'affichage
     * @param dividend le nombre de réponses de ce choix
     * @return le pourcentage des réponses qui ont répondues ce choix
     */
    private fun getPercent(dividend: Int) : Int {
        val divider : Float = askQuestion.response.nbChoice1 + askQuestion.response.nbChoice2.toFloat()
        if(divider > 0f)
        {
            val percent : Float = (dividend / divider) * MULTIPLIER
            val percentRound = percent.toInt()
            return if (percent - percentRound < 0.5f)
                percentRound
            else
                percentRound + 1

        }
        return 0
    }

    /**
     * La visibilité de la progressBar
     */
    val progressBarView: Int
        @Bindable
        get() { return getVisibility(AskQuestionActivityState.LOADING) }

    /**
     * La visibilité du "layout" réponse
     */
    val answerView: Int
        @Bindable
        get() { return getVisibility(AskQuestionActivityState.DISPLAYRESULTS) }

    /**
     * La visibilité du "layout" question
     */
    val questionView: Int
        @Bindable
        get() { return getVisibility(AskQuestionActivityState.ASKQUESTION) }

    /**
     * La visibilité du "layout" erreur
     */
    val errorView: Int
        @Bindable
        get() { return getVisibility(AskQuestionActivityState.ERROR_RETRY) }

    /**
     * Détermine si la vue doit être visible ou non selon l'état
     * @param view la vue en question (état qui le représente)
     * @return La visibilité souhaité
     */
    private fun getVisibility(view : AskQuestionActivityState) : Int {
        return if(view == currentState)
            VISIBLE
        else
            INVISIBLE
    }
}

/**
 * Adapteur permettant de modifier la visibilité d'une vue à l'aide des "Binding"
 * @param view la vue dont l'on veut modifier la visibilité
 * @param visibility la visibilité que l'on veut assigner à la vue
 */
@BindingAdapter("android:visibility")
fun setVisibility(view: View, visibility : Int) {
    view.visibility = visibility
}