package ca.csf.mobile2.tp2.question.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Objet servant de modèle permettant d'obtenir les informations nescéssaires à l'affichage
 * @property currentState l'état actuel du modèle
 * @property response la question à afficher
 */
@Parcelize
class AskQuestion(var currentState: AskQuestionActivityState,
                  var response : Question
) : Parcelable {
}