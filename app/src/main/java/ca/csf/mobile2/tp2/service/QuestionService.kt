package ca.csf.mobile2.tp2.service

import ca.csf.mobile2.tp2.exception.QuestionServiceException
import ca.csf.mobile2.tp2.question.data.Question
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.androidannotations.annotations.EBean
import retrofit2.http.GET
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.POST
import retrofit2.http.Path
import java.net.SocketTimeoutException

/**
 *  Constante qui représente l'URL de base utilisé pour accéder au service web
 */
private const val URL = "http://10.0.2.2:8080/"

/**
 * Classe qui représente l'interraction application mobile vers le service web
 */
@EBean(scope = EBean.Scope.Singleton)
class QuestionService {

    /**
     * Interface qui représente le service qui sera utilisé pour communiquer avec le service web
     * à partir de l'application.
     */
    interface Service {
        @GET("/api/v1/question/random")
        fun getRandomQuestion() : Call<Question>

        @POST("/api/v1/question/{id}/choose1")
        fun chooseFirstChoice(@Path("id") id: String) : Call<Question>

        @POST("/api/v1/question/{id}/choose2")
        fun chooseSecondChoice(@Path("id") id: String) : Call<Question>

        @GET("/api/v1/question/{id}")
        fun getSpecificQuestion(@Path("id") id: String) : Call<Question>
    }

    /**
     * Représente le service qui sera utilisé pour communiquer avec le service web
     * à partir de l'application.
     */
    private val service : Service
    init{
        val jackson = ObjectMapper().registerKotlinModule()

        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(JacksonConverterFactory.create(jackson))
            .build()

        service = retrofit.create(Service::class.java)
    }

    /**
     * Méthode qui permet de recevoir du serveur une question aléatoire
     * @return un objet de type Question qui représente la question qui sera demandée à l'utilisateur
     * @exception SocketTimeoutException si il est impossible de se connecter aux ports du serveur
     * @throws QuestionServiceException lorsque la connexion au serveur échoue
     */
    fun getRandomQuestion() : Question {
        val call: Call<Question> = service.getRandomQuestion()

        try{
            val response: Response<Question> = call.execute()

            if(response.isSuccessful) {
                var result = response.body()
                if(result != null) {
                    return result
                }
            }
            throw QuestionServiceException(response.message().toString())

        }catch (e:SocketTimeoutException){
            throw QuestionServiceException(e.toString())
        }
    }

    /**
     * Méthode qui permet d'envoyer notre vote pour le premier choix au serveur
     * @param id représente le numéro d'identification de la question
     * @exception SocketTimeoutException si il est impossible de se connecter aux ports du serveur
     * @return un objet de type Question qui représente la question à laquelle on répond
     * @throws QuestionServiceException lorsque la connexion au serveur échoue
     */
    fun chooseFirstChoice(id:String) : Question {
        val call: Call<Question> = service.chooseFirstChoice(id)

        try{
            val response: Response<Question> = call.execute()

            if(response.isSuccessful) {
                var result = response.body()
                if(result != null) {
                    return result
                }
            }
            throw QuestionServiceException(response.message().toString())

        }catch (e:SocketTimeoutException){
            throw QuestionServiceException(e.toString())
        }
    }

    /**
     * Méthode qui permet d'envoyer notre vote pour le second choix au serveur
     * @param id représente le numéro d'identification de la question
     * @exception SocketTimeoutException si il est impossible de se connecter aux ports du serveur
     * @return un objet de type Question qui représente la question à laquelle on répond
     * @throws QuestionServiceException lorsque la connexion au serveur échoue
     */
    fun chooseSecondChoice(id:String) : Question {
        val call: Call<Question> = service.chooseSecondChoice(id)

        try{
            val response: Response<Question> = call.execute()

            if(response.isSuccessful) {
                var result = response.body()
                if(result != null) {
                    return result
                }
            }
            throw QuestionServiceException(response.message().toString())

        }catch (e:SocketTimeoutException){
            throw QuestionServiceException(e.toString())
        }
    }

    /**
     * Méthode qui permet de demander au serveur une question spécifique selon son id
     * @param id représente le numéro d'identification de la question
     * @exception SocketTimeoutException si il est impossible de se connecter aux ports du serveur
     * @return un objet de type Question qui représente la question recherchée
     * @throws QuestionServiceException lorsque la connexion au serveur échoue
     */
    fun getSpecificQuestion(id:String) : Question {
        val call: Call<Question> = service.getSpecificQuestion(id)
        try{
            val response: Response<Question> = call.execute()

            if(response.isSuccessful) {
                var result = response.body()
                if(result != null) {
                    return result
                }
            }
            throw QuestionServiceException(response.message().toString())

        }catch (e:SocketTimeoutException){
            throw QuestionServiceException(e.toString())
        }
    }


}