package ca.csf.mobile2.tp2.question

import androidx.appcompat.app.AppCompatActivity
import ca.csf.mobile2.tp2.ActivityMainBinding
import ca.csf.mobile2.tp2.R
import org.androidannotations.annotations.*
import ca.csf.mobile2.tp2.exception.QuestionServiceException
import ca.csf.mobile2.tp2.question.data.AskQuestion
import ca.csf.mobile2.tp2.question.data.AskQuestionActivityState
import ca.csf.mobile2.tp2.question.data.Choice
import ca.csf.mobile2.tp2.question.data.Question
import ca.csf.mobile2.tp2.service.QuestionService

/**
 * Classe qui représente l'activité où les questions sont posées et répondues
 */
@DataBound
@EActivity(R.layout.activity_ask_question)
class AskQuestionActivity : AppCompatActivity() {

    /**
     * Représente l'instance de QuestionService, soit notre lien vers le serveur web
     */
    @Bean
    protected lateinit var questionService: QuestionService

    /**
     * Représente les bindables qui se trouve dans le fichier xml
     */
    @BindingObject
    protected lateinit var binding: ActivityMainBinding

    /**
     * Représente le modèle-vue de l'application
     */
    @InstanceState
    protected lateinit var questionViewModel: QuestionViewModel
    /**
     * Représente le choix que l'utilisateur prend pour une question
     */
    @InstanceState
    protected lateinit var chosenChoice: Choice
    /**
     * Représente le dernier état de l'application avant une erreur
     */
    @InstanceState
    protected lateinit var stateBeforeError: AskQuestionActivityState

    /**
     * Méthode qui permet de mentionner que le choix effectué est le premier choix
     */
    @Click(R.id.choice1Button)
    protected fun onFirstChoiceClicked(){
        selectAnswer(Choice.FIRSTCHOICE)
    }

    /**
     * Méthode qui permet de mentionner que le choix effectué est le second choix
     */
    @Click(R.id.choice2Button)
    protected fun onSecondChoiceClicked(){
        selectAnswer(Choice.SECONDCHOICE)
    }

    /**
     * Méthode qui permet de recevoir une autre question lorsqu'on clique sur le premier résultat
     */
    @Click(R.id.choice1ResultBackground)
    protected fun onFirstResultClicked(){
        askForAnotherQuestion()
    }

    /**
     * Méthode qui permet de recevoir une autre question lorsqu'on clique sur le second résultat
     */
    @Click(R.id.choice2ResultBackground)
    protected fun onSecondResultClicked(){
        askForAnotherQuestion()
    }

    /**
     * Méthode qui permet de recommencer la dernière action entreprise par l'application avant le cas d'erreur
     */
    @Click(R.id.retryButton)
    protected fun onBtnRetryClicked(){
        questionViewModel.currentState = AskQuestionActivityState.LOADING
        when (stateBeforeError) {
            AskQuestionActivityState.DISPLAYRESULTS -> displayResults()
            AskQuestionActivityState.ANSWERQUESTION -> chooseChoice()
            AskQuestionActivityState.ASKQUESTION -> getRandomQuestion()
            else -> getRandomQuestion()
        }
    }

    /**
     * Méthode qui permet d'initialiser les composantes après l'injection de la vue
     */
    @AfterViews
    protected fun onViewsInjected() {
        if (!this::questionViewModel.isInitialized){
            questionViewModel = QuestionViewModel(
                AskQuestion(
                    AskQuestionActivityState.LOADING,
                    Question(
                        "",
                        "",
                        "",
                        0,
                        0,
                        ""
                    )
                )
            )
            getRandomQuestion()
        }
        if(!this::chosenChoice.isInitialized){
            chosenChoice = Choice.FIRSTCHOICE
        }
        if(!this::stateBeforeError.isInitialized){
            stateBeforeError = AskQuestionActivityState.ERROR_RETRY
        }

        binding.questionData = questionViewModel
    }

    /**
     * Méthode qui permet de recevoir une question aléatoire
     * @exception QuestionServiceException si la connexion vers le serveur est impossible
     */
    @Background
    protected fun getRandomQuestion(){
        try {
            questionViewModel.question = questionService.getRandomQuestion()
            questionViewModel.currentState = AskQuestionActivityState.ASKQUESTION
        } catch (e: QuestionServiceException) {
            stateBeforeError = AskQuestionActivityState.ASKQUESTION
            questionViewModel.currentState = AskQuestionActivityState.ERROR_RETRY
        }
    }

    /**
     * Méthode qui permet choisir un choix et de l'envoyer au serveur
     * @exception QuestionServiceException si la connexion vers le serveur est impossible
     */
    @Background
    protected fun chooseChoice(){
        try{
            if(chosenChoice == Choice.FIRSTCHOICE){
                questionService.chooseFirstChoice(questionViewModel.question.id)
            }
            else{
                questionService.chooseSecondChoice(questionViewModel.question.id)
            }
            displayResults()

        } catch (e: QuestionServiceException) {
            stateBeforeError = AskQuestionActivityState.ANSWERQUESTION
            questionViewModel.currentState = AskQuestionActivityState.ERROR_RETRY
        }
    }

    /**
     * Méthode qui permet d'afficher les résultats d'une question
     * @exception QuestionServiceException si la connexion vers le serveur est impossible
     */
    @Background
    protected fun displayResults(){
        try{
            questionViewModel.question = questionService.getSpecificQuestion(questionViewModel.question.id)
            questionViewModel.currentState = AskQuestionActivityState.DISPLAYRESULTS

        } catch (e: QuestionServiceException) {
            stateBeforeError = AskQuestionActivityState.DISPLAYRESULTS
            questionViewModel.currentState = AskQuestionActivityState.ERROR_RETRY
        }
    }

    /**
     * Méthode qui permet déterminer le choix à envoyer comme réponse
     */
    private fun selectAnswer(choice: Choice) {
        chosenChoice = choice
        questionViewModel.currentState = AskQuestionActivityState.LOADING
        chooseChoice()
    }

    /**
     * Méthode qui permet de recevoir une autre question
     */
    private fun askForAnotherQuestion() {
        questionViewModel.currentState = AskQuestionActivityState.LOADING
        getRandomQuestion()
    }

}


