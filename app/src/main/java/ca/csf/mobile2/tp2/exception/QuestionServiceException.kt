package ca.csf.mobile2.tp2.exception

/**
 * Exception provenant de QuestionService
 * @param message la description/le message transmit à l'exception
 */
class QuestionServiceException(message : String) : Exception(message) {
}